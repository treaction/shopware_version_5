{extends file="parent:frontend/index/footer-navigation.tpl"}
{block name="frontend_index_footer_column_newsletter_privacy"}
    {if empty($dataProtectionUrl) }
        {include file="frontend/_includes/privacy.tpl"}
    {else}
        <label>
            <input type="checkbox" name="frontend_newsletter_form_privacy" required="required" aria-required="true" class="input--field is--required has--error">
            Ich stimme den <a href="{$dataProtectionUrl}" target="_blank">Datenschutzbestimmungen</a> zu.
        </label>
        <br>
    {/if}
{/block}