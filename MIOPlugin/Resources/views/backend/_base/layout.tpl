<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{*    <link rel="stylesheet" href="{link file="backend/_resources/css/bootstrap.min.css"}">*}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {*<link rel="stylesheet" href="{link file="backend/_resources/css/style.css"}">*}
</head>
<body role="document" style="padding-top: 80px">

<!-- Fixed navbar -->
{*<nav class="navbar navbar-fixed-top">
    <div class="container">
        <nav class="navbar navbar-expand-sm navbar-light bg-light" style ="width:100%">
            <a class="navbar-brand" href="#">
                <img style="background-color:black; height:50px; width:50px" src="https://dev-campaign-in-one.net/assets/img/cio/cio-logo-weiss-150x150.png" width="30" height="30" /> MIO Plugin
            </a>
            <ul class="navbar-nav">
                <li{if {controllerAction} === 'index'} class=" nav-link active"{/if}>
                    <a href="{url controller="Manager" action="index" __csrf_token=$csrfToken}">Home</a>
                </li>
*}{*                <li{if {controllerAction} === 'user_registration'} class="active"{/if}><a href="{url controller="Manager" action="user_registration" __csrf_token=$csrfToken}">User Registration</a></li>*}{*
*}{*                <li{if {controllerAction} === 'about_plugin'} class="nav-link "{/if}>
                    <a href="{url controller="Manager" action="about_plugin" __csrf_token=$csrfToken}">About Plugin</a>
                </li>*}{*
            </ul>
        </nav>

*}{*        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="https://treaction.net" target="_blank">
                <img class="header-logo" style="background-color:black; height:50px; width:50px" src="https://dev-campaign-in-one.net/assets/img/cio/cio-logo-weiss-150x150.png" alt="treaction logo">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse">
            <ul class="nav navbar-nav">
                <li{if {controllerAction} === 'index'} class="active"{/if}><a href="{url controller="Manager" action="index" __csrf_token=$csrfToken}">Home</a></li>
                *}{**}{*<li{if {controllerAction} === 'user_registration'} class="active"{/if}><a href="{url controller="Registration" action="user_registration" __csrf_token=$csrfToken}">User Registration</a></li>*}{**}{*
                <li{if {controllerAction} === 'about_plugin'} class="active"{/if}><a href="{url controller="Manager" action="about_plugin" __csrf_token=$csrfToken}">About Plugin</a></li>

            </ul>
        </div><!--/.nav-collapse -->*}{*
    </div>
</nav>*}

<div id="main_win" class="container theme-showcase" role="main">
    {block name="content/main"}{/block}
</div> <!-- /container -->

<script type="text/javascript" src="{link file="backend/base/frame/postmessage-api.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/jquery-2.1.4.min.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/bootstrap.min.js"}"></script>
{*<script>
    $(document).ready(function () {

        $('.btn').on('click', function() {
            var $this = $(this);
            $this.button('loading');
        });
    });*}
</script>
</body>
</html>
