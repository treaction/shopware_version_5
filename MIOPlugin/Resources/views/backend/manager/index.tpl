{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
    <br>
    <div class="mio-container">
        {*Plugin Activation Status*}
        {if $is_plugin_activated}
            <div class="alert alert-primary" role="alert">
                Plugin is Active
            </div>
        {else}
            <div class="card">
                <div class="card-header">
                    Warning
                </div>
                <div class="card-body">
                    <h5 class="card-title">Plugin is not active</h5>
                    <p class="card-text">Please register to Marketing-In-One to obtain free APIKey.</p>
                    {*   <a href="#" class="btn btn-primary">Go somewhere</a>*}
                </div>
            </div>
        {/if}
        <br>
        {*New User Registration for Marketing-In-One*}
        {if $require_registration }
            <form id="import_all" class="form-horizontal" method="post"
                  action="{url controller="Manager" action="user_registration" __csrf_token=$csrfToken}">
                <div class="card">
                    <div class="card-header">
                        <h3>Marketing-In-One Account Registration</h3>
                    </div>
                    <div class="card-body">
                        <br>
                        <div class="card-text">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="mb-4 col-lg-12">
                                            <label for="nw-cus-sal" class="text-left w-100"
                                                   hidden>Salutation</label>
                                            <select class="form-control" name="salutation" id="nw-cus-sal">
                                                <option class="form-control" value="" selected disabled>
                                                    Salutation
                                                </option>
                                                <option class="form-control" value="Mr">Mr</option>
                                                <option class="form-control" value="Mrs">Mrs</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--FirstName-->
                                        <div class="mb-4 col-lg-12">
                                            <label for="nw-cus-fstnme" class="text-left w-100" hidden>First
                                                Name</label>
                                            <input type="text" name="firstname" class="form-control"
                                                   id="nw-cus-fstnme"
                                                   placeholder="First Name" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--LastName-->
                                        <div class="mb-4 col-lg-12">
                                            <label for="nw-cus-lstnme" class="text-left w-100" hidden>Last
                                                Name</label>
                                            <input type="text" name="lastname" class="form-control"
                                                   id="nw-cus-lstnme"
                                                   placeholder="Last Name" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--Email-->
                                        <div class="mb-4 col-lg-12">
                                            <label for="nw-cus-em" class="text-left w-100" hidden>E-Mail</label>
                                            <input type="email" name="email" class="form-control" id="nw-cus-em"
                                                   placeholder="E-Mail"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card" style="margin-bottom:15px;">
                                        <div class="card-header">
                                            <label class="mb-4" style="margin-bottom: 5px;">
                                                <input type="checkbox" id="nw-cus-permission" name="permission"
                                                       required>
                                                <small>&nbsp;&nbsp;Ich erkläre mich mit den <a
                                                            href="https://treaction.net/rechtliches/#agb"
                                                            target="_blank">AGB</a>
                                                    und <a
                                                            href="https://treaction.net/rechtliches/#datenschutz"
                                                            target="_blank">Datenschutzbestimmungen</a>
                                                    der treaction AG, Kurfürstenstr. 1, 34117 Kassel,
                                                    einverstanden. Ich bin
                                                    einverstanden, dass die treaction AG meine eingetragenen
                                                    Daten nutzt und mir
                                                    E-Mails
                                                    zuschickt bzw. mich anruft, um mir Angebote/Beratung zu den
                                                    Bereichen
                                                    Lead-Generierung, Marketing-Automation oder E-Mail-Marketing
                                                    zukommen zu
                                                    lassen.
                                                    Diese Einwilligung kann ich jederzeit widerrufen, etwa durch
                                                    einen Brief an
                                                    die oben
                                                    genannte Adresse oder durch eine E-Mail an <a
                                                            href="mailto:widerruf@treaction.net">widerruf@treaction.net</a>.
                                                    Anschließend wird jede werbliche Nutzung
                                                    unterbleiben.</small>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" id="load" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            </form>
        {/if}

        {*MIO PLUGIN DETAILS*}
        {if $mio_plugin}
            <div class="card">
                <div class="card-header">
                    MIOPLUGIN Details
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <dl class="row">

                            <dt class="col-sm-2">Account Number</dt>
                            <dd class="col-sm-9">{$mio_plugin.accountno}</dd>

                            <dt class="col-sm-2">APIKey</dt>
                            <dd class="col-sm-9">{$mio_plugin.apikey}</dd>

                            <dt class="col-sm-2">Status</dt>
                            <dd class="col-sm-9">{$webhooks.message}</dd>

                            <dt class="col-sm-2"></dt>
                            <dd class="col-sm-9"></dd>

                            <dt class="col-sm-2">List of Webhooks</dt>
                            <dd class="col-sm-9">{''} </dd>
                            <dt class="col-sm-2">{$webhooks.response[0].name}</dt>
                            <dd class="col-sm-9">
                                <dl class="row">

                                    <dd class="col-sm-8">{$webhooks.response[0].url}</dd>

                                    <dd class="col-sm-8">{$webhooks.response[0].id}</dd>
                                </dl>
                            </dd>
                            <dt class="col-sm-2">{$webhooks.response[1].name}</dt>
                            <dd class="col-sm-9">
                                <dl class="row">

                                    <dd class="col-sm-8">{$webhooks.response[1].url}</dd>

                                    <dd class="col-sm-8">{$webhooks.response[1].id}</dd>
                                </dl>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        {/if}

    </div>
{/block}
