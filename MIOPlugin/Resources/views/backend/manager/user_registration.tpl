{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
<div class="mio-container">
    {if !empty($success)}
        <div class="alert alert-primary" role="alert">
            {$success}
        </div>
    {/if}
    {if !empty($error)}
        <div class="alert alert-danger" role="alert">
            {$success}
        </div>
    {/if}
    <a class="float-right" href="{url controller="Manager" action="index" __csrf_token=$csrfToken}">Back</a>
</div>


{/block}