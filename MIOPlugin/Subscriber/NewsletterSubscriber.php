<?php

namespace MIOPlugin\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use MIOPlugin\Components\MIOService\NewsletterHook;
use MIOPlugin\Components\PluginLogger;
use Shopware\Components\Plugin\ConfigReader;
use Shopware_Controllers_Frontend_Newsletter;

class NewsletterSubscriber implements SubscriberInterface
{
    /**
     * @var string
     * @author Pradeep
     */
    private $pluginName;
    /**
     * @var ConfigReader
     * @author Pradeep
     */
    private $configReader;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var NewsletterHook
     * @author Pradeep
     */
    private NewsletterHook $newsletterHook;

    /**
     * - all the arguments are loaded by Shopware dependency injection.
     * - Service has to be initialized in services.xml.
     * NewsletterSubscriber constructor.
     * @param string $pluginName
     * @param ConfigReader $configReader
     * @param NewsletterHook $newsletterHook
     */
    public function __construct(string $pluginName, ConfigReader $configReader, NewsletterHook $newsletterHook)
    {
        $this->pluginName = $pluginName;
        $this->configReader = $configReader;
        $this->newsletterHook = $newsletterHook;
        $this->logger = new PluginLogger();
    }


    /**
     * - getSubscribedEvents is the shopware function.
     * - returns the list of events subscribed.
     * -- 'Enlight_Controller_Action_PreDispatch_Frontend_Newsletter'
     *                                => Newsletter Subscription Page
     * -- 'Enlight_Controller_Action_PostDispatchSecure_Frontend'
     *                                => Changing default 'Datenschutzbestimmungen' to
     * checkbox
     * @return array
     * @author Pradeep
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_Newsletter' => 'preDispatchNewsletter',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend'=>'postDispatchFrontend'
        ];
    }

    /**
     * - postDispatchFrontend loads the frontend template in path 'views/frontend/index/footer-navigation.tpl
     * - template replaces default 'Datenschutzbestimmungen' to required checkbox.
     * @param Enlight_Event_EventArgs $args
     * @return bool
     * @author Pradeep
     */
    public function postDispatchFrontend(Enlight_Event_EventArgs $args): bool
    {
        $args->getSubject()->View()->addTemplateDir(__DIR__.'/../Resources/views');
        $view = $args->getSubject()->View();
        $pluginConfig = $this->getPluginConfig();

        $apikey = trim($pluginConfig[ 'apikey' ]);
        $accountNo = trim($pluginConfig[ 'accountno' ]);

        if (!$this->newsletterHook->initialize($apikey, (int)$accountNo)) {
            $this->logger->addLog('Error', 'Failed to initialize MIOService', __CLASS__, __METHOD__, __LINE__);
            return false;
        }

        $company = $this->newsletterHook->getCompanyPrivacyPolicyFromMIO();
        if(!empty($company)) {
            $shopName = $company['name'];
            $imprint = $company['url_imprint'];
            $shopPrivacyPolicy = $company['url_privacy_policy'];
            // Update the view of Newsletter Template
            $view->assign('shopName',trim($shopName));
            $view->assign('imprint',trim($imprint));
            $view->assign('dataProtectionUrl',trim($shopPrivacyPolicy));
        }

        return true;
    }

    /**
     * - preDispatchNewsletter is the newsletter event.
     * - The event is called everytime the newsletter page is opened in the frontend.
     * - the event gathers the post data if present and initializes the connection with AIO.
     * @param Enlight_Event_EventArgs $args
     * @return bool
     * @author Pradeep
     */
    public function preDispatchNewsletter(Enlight_Event_EventArgs $args): bool
    {
        /** @var Shopware_Controllers_Frontend_Newsletter $subject */
        $args->getSubject()->View()->addTemplateDir(__DIR__.'/../Resources/views');
        $request = $args->getRequest();
        $pluginConfig = $this->getPluginConfig();
        $view = $args->getSubject()->View();

        $post = $request->getPost();
        $this->logger->addLog('post Info', json_encode($post), __CLASS__, __METHOD__, __LINE__);

        $apikey = trim($pluginConfig[ 'apikey' ]);
        $accountNo = trim($pluginConfig[ 'accountno' ]);

        if (empty($pluginConfig) || !isset($apikey, $accountNo)) {
            $this->logger->addLog('Failed', json_encode(['post' => $post, 'pluginConfig' => $pluginConfig]), __CLASS__,
                __METHOD__, __LINE__);
            return false;
        }

        if (!$this->newsletterHook->initialize($apikey, (int)$accountNo)) {
            $this->logger->addLog('Error', 'Failed to initialize MIOService', __CLASS__, __METHOD__, __LINE__);
            return false;
        }
        // fetch data from MIO
        $company = $this->newsletterHook->getCompanyPrivacyPolicyFromMIO();
        $this->logger->addLog('info newsletter copany', json_encode($company));
        if(!empty($company)) {
            $shopName = $company['name'];
            $imprint = $company['url_imprint'];
            $shopPrivacyPolicy = $company['url_privacy_policy'];
            // Update the view of Newsletter Template
            $view->assign('shopName',trim($shopName));
            $view->assign('imprint',trim($imprint));
            $view->assign('dataProtectionUrl',trim($shopPrivacyPolicy));
        }

        if ($this->isValidPost($post)) {
            $this->newsletterHook->sendToNLWebhook($post);
        }

        return true;
    }

    /**
     * - getPluginConfig is a helper function to fetch the plugin configuration
     * - fetches the apikey and account number
     * - apikey and account are necessary to build the connection with AIO.
     * @return array|string[]
     * @author Pradeep
     */
    private function getPluginConfig(): array
    {
        $configReader = $this->getConfigReader();
        $pluginName = $this->getPluginName();
        if ($configReader === null || empty($pluginName)) {
            $this->logger->addLog('error', 'Invalid ConfigReader or PluginName provided');
            return [];
        }
        return $configReader->getByPluginName($pluginName);
    }

    /**
     * - getConfigReader helper function to read the plugin config.
     * @return ConfigReader|null
     * @author Pradeep
     */
    private function getConfigReader(): ?ConfigReader
    {
        if (!$this->configReader instanceof ConfigReader) {
            return null;
        }
        return $this->configReader;
    }

    /**
     * - getPluginName is helper function to check if the plugin name is set or not.
     * - pluginName is set using dependency injections from sevices.xml.
     * @return string
     * @author Pradeep
     */
    private function getPluginName(): string
    {
        if (empty($this->pluginName)) {
            return '';
        }
        return $this->pluginName;
    }

    /**
     * - isValidPost validation function for validating the newsletter subscribtion data.
     * - the validation is done before sending the data to AIO.
     * @param array $postData
     * @return bool
     * @author Pradeep
     */
    private function isValidPost(array $postData): bool
    {
        if (empty($postData) || empty($postData[ 'newsletter' ])) {
            $this->logger->addLog('error', 'Invalid PostData' . json_encode($postData), __CLASS__, __METHOD__,
                __LINE__);
            return false;
        }
        $isValidEmail = filter_var(trim($postData[ 'newsletter' ]), 'FILTER_VALIDATE_EMAIL');
        if (is_bool($isValidEmail) && $isValidEmail === false) {
            $this->logger->addLog('error', 'Invalid Email Address ' . json_encode($postData[ 'newsletter' ]), __CLASS__,
                __METHOD__, __LINE__);
            return false;
        }
        return true;
    }

}