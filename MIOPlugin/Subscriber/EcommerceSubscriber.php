<?php

namespace MIOPlugin\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Hook_HookArgs;
use MIOPlugin\Components\MIOService;
use MIOPlugin\Components\MIOService\ECommerceHook;
use MIOPlugin\Components\PluginLogger;
use MIOPlugin\Components\ShopOrders;
use MIOPlugin\Models\BasketRepository;
use MIOPlugin\Models\CustomerRepository;
use sAdmin;
use Shopware\Components\Api\Resource\Customer;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Customer\Repository;
use Shopware\Models\Order\Order;
use Shopware_Controllers_Frontend_Account;
use Symfony\Component\HttpFoundation\RequestStack;

class EcommerceSubscriber implements SubscriberInterface
{

    /**
     * @var ConfigReader
     * @author Pradeep
     */
    private $configReader;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var
     * @author Pradeep
     */
    private $sUserData;
    /**
     * @var
     * @author Pradeep
     */
    private $sBasket;
    /**
     * @var ModelManager
     * @author Pradeep
     */
    private $modelManager;
    /**
     * @var sAdmin
     * @author Pradeep
     */
    private $admin;
    /**
     * @var string
     * @author Pradeep
     */
    private $pluginName;
    /**
     * @var array
     * @author Pradeep
     */
    private $sBasketData;
    /**
     * @var CustomerRepository
     * @author Pradeep
     */
    private $customer;
    /**
     * @var BasketRepository
     * @author Pradeep
     */
    private $basket;
    /**
     * @var ShopOrders
     * @author Pradeep
     */
    private ShopOrders $shopOrders;
    /**
     * @var RequestStack
     * @author Pradeep
     */
    private RequestStack $requestStack;

    public function __construct(string $pluginName, ConfigReader $configReader, ModelManager $modelManager,RequestStack $requestStack)
    {
        $this->pluginName = $pluginName;
        $this->modelManager = $modelManager;
        $this->configReader = $configReader;
        $this->logger = new PluginLogger();
        $this->customer = new CustomerRepository();
        $this->basket = new BasketRepository();
        $this->shopOrders = new ShopOrders();
        $this->admin = $this->getAdmin();
        $this->requestStack = $requestStack;
    }

    /**
     * @return sAdmin
     * @author Pradeep
     * @internal Get Shopware Logged In User Data
     */
    private function getAdmin()
    {
        return Shopware()->Modules()->Admin();
    }

    /**
     * @return array
     * @author Pradeep
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout'=> 'postDispatchECommerce',
            'Shopware_Controllers_Frontend_Checkout::finishAction::after' => 'afterFinishAction',
        ];
    }

    /**
     * @param Enlight_Event_EventArgs $args
     * @author Pradeep
     * @internal This Subscriber is responsible for autofilling checkboxes in checkout Page ( Check in View folder)
     */
    public function postDispatchECommerce(Enlight_Event_EventArgs $args): bool
    {
        $pluginConfig = $this->getPluginConfig();
        $this->logger->addLog('Info Plugin Details', json_encode($pluginConfig));
        // trim apikey and account Number.
        $apikey = trim($pluginConfig[ 'apikey' ]);
        $accountNo = trim($pluginConfig[ 'accountno' ]);
        $email = trim($pluginConfig[ 'email' ]) ?? '';

        // adding template path
        $args->getSubject()->View()->addTemplateDir(__DIR__.'/../Resources/views');
        $view = $args->getSubject()->View();
        $eCommerceHook = new ECommerceHook();
        if (!isset($apikey, $accountNo) || !$eCommerceHook->initialize($apikey, (int)$accountNo)) {
            $this->logger->addLog('Error', 'Failed to initialize MIOService', __CLASS__, __METHOD__, __LINE__);
            return false;
        }

        $company = $eCommerceHook->getCompanyPrivacyPolicyFromMIO();
        $this->logger->addLog('Info Compan detial', json_encode($company));

        if(!isset($company['name'], $company['website'],$email) ||
           empty($company) || empty($pluginConfig)) {
            $this->logger->addLog(
                'error',
                'missing shopurl or shopname or Email in plugin config. '. json_encode($pluginConfig));
            return false;
        }

        $shopName = $company['name'];
        $imprint = $company['url_imprint'];
        $shopPrivacyPolicy = $company['url_privacy_policy'];

        $view->assign('shopName',trim($shopName));
        $view->assign('imprint',trim($imprint));
        $view->assign('email',$email);
        $view->assign('dataProtectionUrl',trim($shopPrivacyPolicy));
        return true;
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     * @return bool
     * @author Pradeep
     */
    public function afterFinishAction(Enlight_Hook_HookArgs $args): bool
    {
        if ($this->isPermissionGranted()) {
            $subject = $args->getSubject();
            $userData = $subject->View()->getAssign('sUserData');
            $basketData = $subject->View()->getAssign('sBasket');
            $this->logger->addLog('Info', 'BasketData ' . json_encode($basketData), __CLASS__, __METHOD__, __LINE__);
            $this->logger->addLog('Info', 'UserInfo ' . json_encode($userData), __CLASS__, __METHOD__, __LINE__);
            if (empty($userData) ||
                empty($userData[ 'additional' ][ 'user' ] || (int)$userData[ 'additional' ][ 'user' ][ 'userID' ] <= 0)) {
                $this->logger->addLog('error', 'Invalid User Data retrieved ' . json_encode($userData));
                return false;
            }
            $userId = (int)$userData[ 'additional' ][ 'user' ][ 'userID' ];
            if (!$this->customer->initialize($userId) || !$this->basket->setBasket($basketData)) {
                $this->logger->addLog('error', 'Failed to initialize Customer ');
                return false;
            }
            // Customer Information
            $ecHook[ 'first_name' ] = $this->customer->getFirstName();
            $ecHook[ 'salutation' ] = $this->customer->getSalutation();
            $ecHook[ 'last_name' ] = $this->customer->getLastName();
            $ecHook[ 'email' ] = $this->customer->getEmail();
            $ecHook[ 'street' ] = $this->customer->getStreet();
            $ecHook[ 'postal_code' ] = $this->customer->getPostalCode();
            $ecHook[ 'h_no' ] = $this->customer->getHouseNumber();
            $ecHook[ 'city' ] = $this->customer->getCity();
            $ecHook[ 'country' ] = $this->customer->getCountry();
            $ecHook[ 'birthday' ] = $this->customer->getBirthday();

            // Order Information
            $ecHook[ 'smart_tags' ] = $this->basket->getSmartTags();
            $ecHook[ 'last_order_number' ] = $this->basket->getOrderNumber($userId);
            $ecHook[ 'last_order_date' ] = $this->basket->getLastOrderDate();
            $ecHook[ 'last_order_net_value' ] = $this->shopOrders->getOrderNetValue($ecHook[ 'last_order_number' ]);
            $ecHook[ 'last_year_order_net_value' ] = $this->shopOrders->getLastYearOrderNetValue($userId);
            $ecHook[ 'total_order_net_value' ] = $this->shopOrders->getTotalOrdersNetValue($userId);
            $ecHook[ 'first_order_date' ] = $this->shopOrders->getFirstOrderDate($userId);

            // Technical Information.
            $ecHook[ 'ip' ] = $this->shopOrders->getOrderIpAddress($ecHook[ 'last_order_number' ]);
            // DOI and OptinTimestamp has to be similar to lastOrderDate as per requirement of MARC.
            $ecHook['doi_ip'] = $ecHook[ 'ip' ] ?? '';
            $ecHook['doi_timestamp'] = $ecHook[ 'last_order_date' ] ?? '';
            $ecHook['optin_ip'] = $ecHook[ 'ip' ] ?? '';
            $ecHook['optin_timestamp'] = $ecHook[ 'last_order_date' ] ?? '';

            $this->logger->addLog('eCommerceHook',json_encode($ecHook));
            $pluginConfig = $this->getPluginConfig();
            $eCommerceHook = new ECommerceHook();

            // trim apikey and account Number.
            $apikey = trim($pluginConfig[ 'apikey' ]);
            $accountNo = trim($pluginConfig[ 'accountno' ]);
            if (!isset($apikey, $accountNo) || !$eCommerceHook->initialize($apikey, (int)$accountNo)) {
                $this->logger->addLog('Error', 'Failed to initialize MIOService', __CLASS__, __METHOD__, __LINE__);
                return false;
            }
            $response = $eCommerceHook->sendToECWebhook($ecHook);
            $this->logger->addLog('Info', 'eCommerceHook Response ' . json_encode($response), __CLASS__, __METHOD__,
                __LINE__);
            return true;
        }
        return true;
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function isPermissionGranted():bool
    {
        $request = $this->requestStack->getCurrentRequest();
        $permission = $request->get('sNewsletter');
        return $permission === 1 || $permission === '1' || $permission === true;
    }

    /**
     * @return array|string[]
     * @author Pradeep
     * @internal get Plugin Configuration
     */
    private function getPluginConfig(): array
    {
        $configReader = $this->getConfigReader();
        $pluginName = $this->getPluginName();
        if ($configReader === null || empty($pluginName)) {
            $this->logger->addLog('error', 'Invalid ConfigReader or PluginName provided');
            return [];
        }
        return $configReader->getByPluginName($pluginName);
    }

    /**
     * @return ConfigReader|null
     * @author Pradeep
     * @internal Get Method for ConfigReader
     */
    private function getConfigReader(): ?ConfigReader
    {
        if (!$this->configReader instanceof ConfigReader) {
            return null;
        }
        return $this->configReader;
    }

    /**
     * @return string
     * @author Pradeep
     * @internal Get Method for Plugin Name
     */
    private function getPluginName(): string
    {
        if (empty($this->pluginName)) {
            return '';
        }
        return $this->pluginName;
    }

}