<?php
namespace MIOPlugin\Models;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="mio_orders_sync")
 */
class MIOOrdersSync extends ModelEntity
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @author Pradeep
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="mio_account_number", type="string", length=50)
     * @author Pradeep
     */
    public $mioAccountNumber;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="last_sync_time_stamp", type="datetime", nullable=false)
     * @author Pradeep
     */
    public $lastSyncTimeStamp;

    /**
     * @var bool
     * @ORM\Column(name="last_sync_status", type="boolean", nullable=false)
     * @author Pradeep
     */
    public $lastSyncStatus;

    /**
     * @var int
     * @ORM\Column(name="last_order_id", type="string", nullable=false)
     * @author Pradeep
     */
    public $lastOrderId;

    /**
     * @var int
     * @ORM\Column(name="first_order_id", type="string", nullable=false)
     * @author Pradeep
     */
    public $firstOrderId;

    /**
     * @var int
     * @ORM\Column(name="orders_count", type="integer", nullable=false)
     * @author Pradeep
     */
    public $ordersCount;

    /**
     * @return int
     * @author Pradeep
     */
    public function getId()
    {
        return  $this->id;
    }

    /**
     * @param $mioAccountNumber
     * @author Pradeep
     */
    public function setMIOAccoutNumber($mioAccountNumber)
    {
        $this->mioAccountNumber = $mioAccountNumber;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getMIOAccoutNumber()
    {
        return $this->mioAccountNumber;
    }

    /**
     * @param $lastSyncTimeStamp
     * @author Pradeep
     */
    public function setLastSyncTimeStamp($lastSyncTimeStamp)
    {
        $this->lastSyncTimeStamp = $lastSyncTimeStamp;
    }

    /**
     * @return DateTimeInterface
     * @author Pradeep
     */
    public function getLastSyncTimeStamp()
    {
        return $this->lastSyncTimeStamp;
    }

    /**
     * @param $lastSyncStatus
     * @author Pradeep
     */
    public function setLastSyncStatus($lastSyncStatus)
    {
        $this->lastSyncStatus = $lastSyncStatus;
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function getLastSyncStatus()
    {
        return $this->lastSyncStatus;
    }


    public function setLastOrderId($lastOrderId)
    {
        $this->lastOrderId = $lastOrderId;
    }

    public function getLastOrderId()
    {
        return $this->lastOrderId;
    }

    public function setFirstOrderId($firstOrderId)
    {
        $this->firstOrderId = $firstOrderId;
    }

    public function getFirstOrderId()
    {
        return $this->firstOrderId;
    }

    public function setOrdersCount($ordersCount)
    {
        $this->ordersCount = $ordersCount;
    }

    public function getOrdersCount()
    {
        return $this->ordersCount;
    }

}