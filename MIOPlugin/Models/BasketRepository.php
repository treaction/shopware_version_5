<?php


namespace MIOPlugin\Models;


use Exception;
use MIOPlugin\Components\PluginLogger;
use Shopware\Components\Model\ModelManager;

class BasketRepository
{

    /**
     * @var
     * @author Pradeep
     */
    private $shoppingCart;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var mixed|object|ModelManager|null
     * @author Pradeep
     */
    private $em;
    /**
     * @var CategoryRepository
     * @author Pradeep
     */
    private $category;
    /**
     * @var \Enlight_Components_Db_Adapter_Pdo_Mysql|null
     * @author Pradeep
     */
    private ?\Enlight_Components_Db_Adapter_Pdo_Mysql $shopwareDB;

    public function __construct()
    {
        $this->category = new CategoryRepository();
        $this->shoppingCart = [];//$shoppingCart;
        $this->logger = new PluginLogger();
        $this->em = Shopware()->Container()->get('models');
        $this->shopwareDB  =Shopware()->DB();
    }

    /**
     * @param array $basket
     * @return bool
     * @author Pradeep
     */
    public function setBasket(array $basket):bool
    {
        if(empty($basket)) {
            return false;
        }
        $this->shoppingCart = $basket;
        return true;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getSmartTags() :string
    {
        $smartTagsAsArr = [];
        $smartTagsAsStr = '';
        try {
            $content = $this->getBasketContent();
            if (empty($content)) {
                throw new Exception('Invalid ShoppingCart '.json_encode($this->shoppingCart));
            }
            $articles = $content;
            foreach ($articles as $article) {
                if (!isset($article[ 'articleID' ]) || (int)$article[ 'articleID' ] <= 0) {
                    continue;
                }
                $articleId = (int)$article[ 'articleID' ];
                $articleSmartTagsAsArr = $this->getSmartTagForArticle($articleId);
                $this->logger->addLog('info','$articleSmartTagsAsArr : Tags ',json_encode($articleSmartTagsAsArr));
                foreach ($articleSmartTagsAsArr as $articleSmartTags) {
                    if (empty($articleSmartTags)) {
                        continue;
                    }
                    if (!in_array($articleSmartTags, $smartTagsAsArr, true)) {
                        $smartTagsAsArr[] = $articleSmartTags;
                    }
                }
            }
            if( is_array($smartTagsAsArr) && !empty($smartTagsAsArr)) {

                $smartTagsAsStr = implode(",", $smartTagsAsArr);
            }
            return $smartTagsAsStr;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__,__LINE__);
            return $smartTagsAsStr;
        }
    }

    /**
     * @param int $articleID
     * @return array
     * @author Pradeep
     */
    private function getSmartTagForArticle(int $articleID) :array
    {
        $this->logger->addLog('info','getSmartTagForArticle : Start ',$articleID);

        $tags = [];
        try {
            if ($articleID <= 0) {
                throw new Exception('Invalid Article ID Provided');
            }
            $path = $this->category->getPath($articleID);
            $this->logger->addLog('info','getSmartTagForArticle : PATH ',json_encode($path));
            $categoryNames = $this->category->convertCategoryPathToNames($path);
            $this->logger->addLog('info','getSmartTagForArticle : CategroyNames '.json_encode($categoryNames));
            return $categoryNames;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__, __LINE__);
            return $tags;
        }
    }


    /**
     * @return array|mixed
     * @author Pradeep
     */
    private function getBasketContent()
    {
        if(empty($this->shoppingCart)) {
            return [];
        }
        return $this->shoppingCart['content'];
    }

    public function getLastOrderNetValue():float
    {
        return $this->getAmountNet();
    }

    public function getLastOrderNumber() {

        if(empty($this->shoppingCart) || !isset($this->shoppingCart['content'][0]['ordernumber'])) {
            return '';
        }
        return $this->shoppingCart['content'][0]['ordernumber'];
    }

    public function getLastOrderDate() {

        if(empty($this->shoppingCart) || !isset($this->shoppingCart['content'][0]['datum'])) {
            return '';
        }
        return $this->shoppingCart['content'][0]['datum'];
    }


    /**
     * @return int
     * @author Pradeep
     */
    private function getOrderQuantity():int
    {
        if(empty($this->shoppingCart) || !isset($this->shoppingCart['Quantity'])) {
            return 0;
        }
        return $this->shoppingCart['Quantity'];
    }

    /**
     * @return float
     * @author Pradeep
     */
    public function getAmountNet():float
    {
        $amountNet = 0.00;
        if(empty($this->shoppingCart) || !isset($this->shoppingCart['AmountNet'])) {
            return $amountNet;
        }
        return (float)$this->shoppingCart['AmountNet'];

    }

    private function getCategoryPath(int $articleId):?array
    {

    }

    public function getOrderNumber(int $userId)
    {
        $stmt = '
                SELECT `ordernumber` 
                FROM `s_order` 
                WHERE `userID` = ' . $userId . ' AND  `ordernumber` > 0
                ORDER BY `ordernumber` DESC LIMIT 1';
        $result = $this->shopwareDB->fetchAll($stmt);
        if(empty($result)) {
            return '';
        }
        return $result[0]['ordernumber'];
    }
}