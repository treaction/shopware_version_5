<?php


namespace MIOPlugin\Models;


use Enlight_Components_Db_Adapter_Pdo_Mysql;
use JsonException;
use MIOPlugin\Components\PluginLogger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\User\User;

class CustomerRepository
{

    /**
     * @var int
     * @author Pradeep
     *
     */
    private $userId;
    /**
     * @var
     * @author Pradeep
     */
    private $firstName;
    /**
     * @var
     * @author Pradeep
     */
    private $salutation;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $lastName;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $email;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $street;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $city;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $country;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $postalCode;
    /**
     * @var
     * @author Pradeep
     */
    private $houseNumber;
    /**
     * @var
     * @author Pradeep
     */
    private $lastOrderNetValue;
    /**
     * @var
     * @author Pradeep
     */
    private $currentOrderDate;
    /**
     * @var
     * @author Pradeep
     */
    private $lastOrderNumber;
    /**
     * @var float
     * @author Pradeep
     */
    private $lastYearOrderNetValue;
    /**
     * @var Enlight_Components_Db_Adapter_Pdo_Mysql|null
     * @author Pradeep
     */
    private $shopwareDB;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var mixed|object|ModelManager|null
     * @author Pradeep
     */
    private $em;
    /**
     * @var mixed|string
     * @author Pradeep
     */
    private $ordertime;


    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->shopwareDB = Shopware()->DB();
        $this->em = Shopware()->Container()->get('models');
    }


    /**
     * - initialize the CustomerRepository
     * - sets UserId, and fetches the UserInformation from DB.
     * @param int $userId
     * @return bool
     * @author Pradeep
     */
    public function initialize(int $userId): bool
    {
        if ($userId <= 0) {
            return false;
        }

        if (!$this->setUserId($userId) || is_null($this->fetchUserInformation())) {
            return false;
        }
        return true;
    }

    /**
     * - setUserId sets the userId.
     * @param int $userId
     * @return bool
     * @author Pradeep
     */
    private function setUserId(int $userId): bool
    {
        $status = false;
        if ($userId > 0) {
            $this->userId = $userId;
            $status = true;
        }
        $this->logger->addLog('info', 'setUserId ' . json_encode($this->userId));
        return $status;
    }

    /**
     * - getBirthday gets birthday for the User/Customer.
     * @return string
     * @author Pradeep
     */
    public function getBirthday()
    {
        if (!empty($this->birthday)) {
            return $this->birthday;
        }
        return '';
    }

    /**
     * - getFirstName gets birthday for the User/Customer.
     * @return string
     * @author Pradeep
     */
    public function getFirstName()
    {
        if (!empty($this->firstName)) {
            return $this->firstName;
        }
        return '';
    }

    /**
     * - getSalutation gets birthday for the User/Customer.
     * @return string
     * @author Pradeep
     */
    public function getSalutation()
    {
        if (!empty($this->salutation)) {
            return $this->salutation;
        }
        return '';
    }

    /**
     * - getLastName gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getLastName()
    {
        if (!empty($this->lastName)) {
            return $this->lastName;
        }
        return '';
    }

    /**
     * - getEmail gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getEmail()
    {
        if (!empty($this->email)) {
            return $this->email;
        }
        return '';
    }

    /**
     * - getStreet gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getStreet()
    {
        if (!empty($this->street)) {
            return $this->street;
        }
        return '';
    }

    /**
     * - getPostalCode gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getPostalCode()
    {
        if (!empty($this->postalCode)) {
            return $this->postalCode;
        }
        return '';
    }

    /**
     * - getHouseNumber gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getHouseNumber()
    {
        if (!empty($this->houseNumber)) {
            return $this->houseNumber;
        }
        return '';
    }

    /**
     * - getCity gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getCity()
    {
        if (!empty($this->city)) {
            return $this->city;
        }
        return '';
    }

    /**
     * - getCountry gets birthday for the User/Customer.
     * @return mixed|string
     * @author Pradeep
     */
    public function getCountry()
    {
        if (!empty($this->country)) {
            return $this->country;
        }
        return '';
    }



    /**
     * - fetchUserInformation get the user information provided by userId.
     * - information is fetched by SQL query from users table.
     * - Mainly used by eCommerceSubscriber while sending information to AIO.
     * @return null
     * @author Pradeep
     */
    public function fetchUserInformation()
    {
        $this->logger->addLog('info', 'fetchUserInformation:Start ' . json_encode($this->userId));
        if ($this->userId === null || $this->userId <= 0 || $this->shopwareDB === null) {
            return null;
        }

        $query = 'SELECT
                    -- Id
                    u.id as user_id,

                    -- User information
                    u.email as email , 
                    u.salutation as anrede ,
                    u.firstname as first_name , 
                    u.lastname as last_name ,
                    u.birthday as birthday,
                    ua.street as street ,
                    ua.zipcode as postal_code,
                    ua.city as city,
                    cc.countryname as country     
                    
                FROM s_user as u 
                JOIN s_user_addresses as ua on ua.user_id = u.id
                JOIN s_core_countries as cc on cc.id = ua.country_id
                WHERE u.id = ' . $this->userId;
        $result = $this->shopwareDB->fetchAll($query);

        if (empty($result) || empty($result[ 0 ])) {
            return null;
        }
        //$result = $result[0];
        $this->salutation = $result[ 0 ][ "anrede" ];
        $this->firstName = $result[ 0 ][ "first_name" ];
        $this->lastName = $result[ 0 ][ "last_name" ];
        $this->email = $result[ 0 ][ "email" ];
        $this->street = $result[ 0 ][ "street" ];
        $this->postalCode = $result[ 0 ][ "postal_code" ];
        $this->city = $result[ 0 ][ "city" ];
        $this->country = $result[ 0 ][ "country" ];
        $this->birthday = $result[ 0 ][ "birthday" ];

        return $result;
    }

    /**
     * @return array|float|int
     * @author Pradeep
     * @deprecated moved to shopOrders Class.
     */
    public function getLastYearOrderNetValue(): ?float
    {
        if (empty($this->userId) || $this->userId <= 0 || $this->shopwareDB === null) {
            return null;
        }
        $userId = $this->userId;
        $lastYear = $this->getLastYearStartAndEndDate();
        $startDate = $lastYear[ 'startDate' ];
        $endDate = $lastYear[ 'endDate' ];
        $netValue = 0;
        $stmt = '
                SELECT `ordernumber`,`invoice_amount_net`,`ordertime` 
                FROM `s_order` 
                WHERE 
                    `ordertime` >= "' . $startDate . '" AND 
                    `ordertime` < "' . $endDate . '" AND 
                    `userID` = ' . $userId . ' AND 
                    `ordernumber` > 0';
        $result = $this->shopwareDB->fetchAll($stmt);
        foreach ($result as $res) {
            if (empty($res)) {
                continue;
            }
            $netValue += (float)$res[ 'invoice_amount_net' ];
        }
        return $netValue;
    }

    /**
     * @return array
     * @author Pradeep
     * @deprecated moved to shopOrders Class.
     */
    private function getLastYearStartAndEndDate(): array
    {
        $currentYear = date('Y');
        $lastYear = $currentYear - 1;
        $startDate = mktime(0, 0, 0, date('m'), date('d'), $lastYear) . "<br/>";
        $endDate = mktime(0, 0, 0, date('m'), date('d'), $currentYear) . "<br/>";
        return [
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
        ];
    }

    /**
     * @return array|null
     * @throws JsonException
     * @author Pradeep
     * @deprecated moved to shopOrders Class.
     */
    private function fetchUserLastOrderInformation(): ?array
    {
        $this->logger->addLog('info', 'fetchUserLastOrderInformation: START  ');
        if ($this->userId <= 0 || $this->shopwareDB === null) {
            return null;
        }

        $query =
            'SELECT `ordernumber`,`invoice_amount_net`,`ordertime` 
                FROM `s_order` 
                WHERE `userID` = ' . $this->userId . ' AND  ordernumber > 0 
                ORDER BY `id` DESC
                LIMIT 2';
        $result = $this->shopwareDB->fetchAll($query);
        $lastOrderDetails = [];
        // Fetch last order details, For new user fetch latest order details.
        if (count($result) > 1 && !empty($result[ 1 ])) {
            $lastOrderDetails = $result[ 1 ];
        } else {
            if (count($result) === 1) {
                $lastOrderDetails = $result[ 0 ];
            }
        }
        $this->logger->addLog('info', 'fetchUserLastOrderInformation: RESULT ' . json_encode($result));
        $this->lastOrderNetValue = isset($lastOrderDetails[ 'invoice_amount_net' ]) ?
            round($lastOrderDetails[ 'invoice_amount_net' ], 2) : 0.00;
        $this->lastOrderNumber = $lastOrderDetails[ 'ordernumber' ] ?? '-';
        $this->lastOrderDate = $lastOrderDetails[ 'ordertime' ] ?? '-';
        $this->logger->addLog('info', 'fetchUserLastOrderInformation: RESULT ' . json_encode([
                'lastOrderNumber' => $this->lastOrderNumber,
                'lastOrderNetValue' => $this->lastOrderNetValue,
            ], JSON_THROW_ON_ERROR));
        return $result;
    }

}