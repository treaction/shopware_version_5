<?php


namespace MIOPlugin\Models;


use MIOPlugin\Components\PluginLogger;

class ArticleRepository
{

    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->em = Shopware()->Container()->get('models');
    }

    /**
     * @param int $articleId
     * @author Pradeep
     * @internal returns article Information.
     */
    public function get(int $articleId)
    {

    }

    /**
     * @param $articleId
     * @author Pradeep
     */
    public function getCategory($articleId)
    {

    }
}