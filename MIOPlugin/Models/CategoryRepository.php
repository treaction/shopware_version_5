<?php


namespace MIOPlugin\Models;


use Exception;
use MIOPlugin\Components\PluginLogger;

class CategoryRepository
{

    /**
     * @var \Enlight_Components_Db_Adapter_Pdo_Mysql|null
     * @author Pradeep
     */
    private $shopwareDB;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var mixed|object|\Shopware\Components\Model\ModelManager|null
     * @author Pradeep
     */
    private $em;
    /**
     * @var array
     * @author Pradeep
     */
    private $shopCategories;

    public function __construct()
    {
        $this->shopCategories = [];
        $this->logger = new PluginLogger();
        $this->shopwareDB = Shopware()->DB();
        $this->em = Shopware()->Container()->get('models');
    }

    /**
     * @return array
     * @author Pradeep
     */
    private function getAll():array
    {
        $categories = [];
        $this->logger->addLog('info','getAll : START ',__CLASS__, __METHOD__,__LINE__);
        try {
            if ($this->shopwareDB === null) {
                throw new Exception('Failed to fetch Shopware database ' . json_encode($this->shopwareDB));
            }
            $stmt = 'SELECT id, description as name FROM `s_categories` order by id ASC';
            $result = $this->shopwareDB->fetchAll($stmt);
            $this->logger->addLog('info','getAll : RESULT '.json_encode($result),__CLASS__, __METHOD__,__LINE__);
            if (!empty($result)) {
                $categories = $result;
            }
            $this->logger->addLog('info','getAll : END '.json_encode($categories),__CLASS__, __METHOD__,__LINE__);
            return $categories;
        } catch (Exception $e) {
            $this->logger->addLog('Exception', $e->getMessage(), __CLASS__, __METHOD__,__LINE__);
            return $categories;
        }
    }

    /**
     * @param string $email
     * @return string
     * @author Pradeep
     * @internal
     */
    public function convertCategoryPathToNames(array $categoryPath):array
    {
        $this->logger->addLog('info','convertCategoryPathToNames : START ',json_encode($categoryPath));
        $smartTags = [];
        if(empty($this->shopCategories) || !isset($this->shopCategories)) {
            $this->shopCategories = $this->getAll();
        }
        $categoryList = $this->shopCategories;
        $this->logger->addLog('info','convertCategoryPathToNames : ShopCategories '.json_encode($this->shopCategories));
        if (empty($categoryList) || empty($categoryPath['parent_category_path'])) {
            return $smartTags;
        }

        $categoryIds = explode('|', $categoryPath['parent_category_path']);
        $smartTags [] = $categoryPath['category_name'];
        foreach ($categoryIds as $categoryId) {
            if (empty($categoryId) || (int)$categoryId <= 0) {
                continue;
            }
            $key = array_search($categoryId, array_column($categoryList, 'id'), true);
            $smartTags [] = $categoryList[ $key ][ 'name' ];
        }
        $this->logger->addLog('info','convertCategoryPathToNames : END ',json_encode($smartTags));
        return $smartTags;
    }

    /**
     * @param int $articleId
     * @return string
     * @author Pradeep
     * @internal returns CategoryPath for an article.
     */
    public function getPath(int $articleId):array
    {
        $this->logger->addLog('info','getPath : START ',$articleId);
        $path = [];
        try {
            if($articleId <= 0) {
                throw new Exception('Invalid Article Id');
            }
            $shopwareDB = Shopware()->DB();
            if($shopwareDB === null) {
                throw new Exception('Shopware DB is null');
            }
            $stmt = '
                    SELECT s_c.id AS id, s_c.path AS parent_category_path, s_c.description as category_name 
                    FROM s_categories as s_c
                    JOIN `s_articles_categories` as s_ac ON s_c.id = s_ac.`categoryID`
                    WHERE s_ac.`articleID` = ' . $articleId;
            $result = $shopwareDB->fetchAll($stmt);
            $this->logger->addLog('info','getPath : Result ',json_encode($result));
            if (!empty($result[0]) && isset($result[0]['id'], $result[0]['parent_category_path'])) {
                $path = $result[0];
            }
            $this->logger->addLog('info','getPath : END ',json_encode($path));
            return $path;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__,__LINE__);
            return $path;
        }
    }

    /**
     * @param int $categoryId
     * @return string
     * @author Pradeep
     * @internal category description
     */
    public function getDescription(int $categoryId):string
    {

    }
}