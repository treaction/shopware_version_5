<?php


namespace MIOPlugin\Models;


use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use MIOPlugin\Components\PluginLogger;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Model\ModelRepository;

class MIOOrdersSyncRepository
{
    /**
     * @var MIOOrdersSync
     * @author Pradeep
     */
    private $mioOrdersSync;
    /**
     * @var mixed|object|ModelManager|null
     * @author Pradeep
     */
    private $em;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;

    public function __construct()
    {
        $this->mioOrdersSync = new MIOOrdersSync();
        $this->em = Shopware()->Container()->get('models');
        $this->logger = new PluginLogger();
    }

    /**
     * @param string $mioAccountNumber
     * @param int $lastOrderId
     * @param int $firstOrderId
     * @param int $ordersCount
     * @param bool $lastSyncStatus
     * @return bool|null
     * @author Pradeep
     */
    public function insert(
        string $mioAccountNumber,
        string $lastOrderId,
        string $firstOrderId,
        int $ordersCount,
        bool $lastSyncStatus
    ): bool {
        try {
            if (empty($mioAccountNumber) || empty($lastOrderId) || empty($firstOrderId) || $ordersCount <= 0
                || empty($lastSyncStatus)) {
                return false;
/*                throw new Exception('Invalid parameter provided ' . json_encode([
                        'mioAccountNumber' => $mioAccountNumber,
                        'lastOrderId' => $lastOrderId,
                        'firstOrderId' => $firstOrderId,
                        'ordersCount' => $ordersCount,
                        'lastSyncStatus' => $lastSyncStatus,
                    ]));*/
            }
            $this->mioOrdersSync->setLastSyncStatus($lastSyncStatus);
            $this->mioOrdersSync->setMIOAccoutNumber($mioAccountNumber);
            $this->mioOrdersSync->setLastOrderId($lastOrderId);
            $this->mioOrdersSync->setFirstOrderId($firstOrderId);
            $this->mioOrdersSync->setOrdersCount($ordersCount);
            $this->mioOrdersSync->setLastSyncTimeStamp($this->getCurrentTimeStamp());
            $this->em->persist($this->mioOrdersSync);
            $this->em->flush($this->mioOrdersSync);
            return true;
        } catch (OptimisticLockException | ORMException | Exception $e) {
            $this->logger->addLog('exception', $e->getMessage(), __CLASS__, __METHOD__, __LINE__);
            return false;
        }
    }

    /**
     * @return false|string
     * @author Pradeep
     * @internal returns current Time stamp.
     */
    private function getCurrentTimeStamp()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * @param string $mioAccountNumber
     * @return bool
     * @author Pradeep
     * @internal Check if the Account Number exists in the table.
     */
    public function isAccountNumberExists(string $mioAccountNumber): bool
    {
        $status = false;

        try {
            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('mioOrderSync.mioAccountNumber')
                ->from(MIOOrdersSync::class, 'mioOrderSync')
                ->where('mioOrderSync.mioAccountNumber = :mioAccountNumber')
                ->setParameter('mioAccountNumber', $mioAccountNumber);
            $data = $queryBuilder->getQuery()->getArrayResult();
            $this->logger->addLog('test', 'isAccountNumberExists '.json_encode($data));
            if (!empty($data)) {
                $status = true;
            }
        } catch (Exception $e) {
            $this->logger->addLog('exception', $e->getMessage(), __CLASS__, __METHOD__, __LINE__);
            return $status;
        }
        return $status;
    }

    public function getLastSyncDetails(): array
    {
        return [];
    }


}