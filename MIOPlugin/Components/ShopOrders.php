<?php


namespace MIOPlugin\Components;


use DateTime;

class ShopOrders
{

    public const ORDER_LIMIT = 10000;
    //private
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var array
     * @author Pradeep
     */
    private $shopOrders;
    /**
     * @var array|false
     * @author Pradeep
     * @internal this value is set from function 'getSmartTags'
     */
    private $shopCategories;
    /**
     * @var array
     * @author Pradeep
     * @internal this value is set from function 'getLatestOrderInformation'
     */
    private $shopOrdersSortedWithDate;

    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->shopOrders = $this->getOrdersFromDB();
    }

    /**
     * @return string
     * @author Pradeep
     * @internal SQL Query for getting Order information from shopware backend.
     */
    private function sqlQuery():string
    {
        return 'SELECT
                    -- Id
                    u.id as user_id,
                    o.id as order_id,
                    od.id as order_details_id,
                    od.articleID as article_id,
                    ac.categoryID as category_id,
                    
                    -- User information
                    u.email as email , 
                    u.salutation as salutation,
                    u.firstname as firstname, 
                    u.lastname as lastname,
                    u.birthday as birthday,
                    ua.street as street,
                    ua.zipcode as postal_code,
                    ua.city as city,
                    cc.countryname as country,
                    
                    -- Order Information
                    o.invoice_amount_net - o.invoice_shipping_net as last_order_net_value,
                    o.ordernumber as order_no,
                    o.remote_addr as ip,
                    o.changed as order_date,
                    
                    -- Category Informtion
                    c.description as categories,
                    c.path as category_path
                    
                    
                FROM s_order as o 
                JOIN s_user as u on u.id =  o.userID
                JOIN s_user_addresses as ua on ua.user_id = u.id
                JOIN s_core_countries as cc on cc.id = ua.country_id
                JOIN s_order_details as od on od.orderID = o.id
                JOIN s_articles_categories ac on ac.articleID = od.articleID
                JOIN s_categories c on c.id = ac.categoryID
                WHERE o.ordernumber != 0
                ORDER BY o.ordernumber ASC';
    }

    /**
     * - getOrdersFromDB is a helper function for the function 'getShopOrderHistory'.
     * - orders are fetched directly from database.
     * @return array
     * @author Pradeep
     */
    private function getOrdersFromDB():array
    {
        $query = $this->sqlQuery();
        return Shopware()->Db()->fetchAll($query);
    }

    /**
     * - getShopOrderHistory gets the complete history of all the orders present till date.
     *
     * @param int $limit
     * @return array
     * @author Pradeep
     */
    public function getShopOrderHistory(int $limit=self::ORDER_LIMIT):array
    {
        $shopOrderHistory = [];
        $orders = $this->shopOrders;
        $temp=[];
        $numberOfOrders = $this->getTotalNumberOfOrders();
        $this->logger->addLog('info','shopOrders '.json_encode($orders));
        foreach($orders as &$order) {
            if(in_array($order[ 'order_no' ], $temp['order_no'], true)) {
                continue;
            }

            $order['smart_tags'] = $this->getSmartTagsForUser($order[ 'user_id' ]); //$this->getSmartTags($order[ 'category_path' ], $order['categories']);
            $order['last_order_date'] = $order['order_date'];
            $order['last_order_no'] = $order['order_no'];
            $order['last_order_net_value'] = round($order['last_order_net_value'],2);
            $order['last_year_order_net_value'] = $this->getLastYearOrderNetValue($order[ 'user_id' ]);
            $order['total_orders_net_value'] = $this->getTotalOrdersNetValue($order[ 'user_id' ]);
            $order[ 'first_order_date' ] = $this->getFirstOrderDate($order[ 'user_id' ]);

            // Technical Information.
            $order['doi_ip'] = $order[ 'ip' ] ?? '';
            $order['doi_timestamp'] = $order[ 'last_order_date' ] ?? '';
            $order['optin_ip'] = $order[ 'ip' ] ?? '';
            $order['optin_timestamp'] = $order[ 'last_order_date' ] ?? '';

            $shopOrderHistory[] = $order;
            $temp['order_no'][] = $order['order_no'];
        }
        unset($order);
        return $shopOrderHistory;
    }

    /**
     * @param int $userId
     * @return string
     * @author Pradeep
     */
    private function getSmartTagsForUser(int $userId):string
    {
        if (empty($userId)) {
            return '';
        }
       // $this->logger->addLog('getSmartTagsForUser', json_encode($userId));
        $path = [];
        $categoryIds =[];
        $description = [];
        $stmt = '
                select 
                       c.path as categoryPath,
                       c.description as description
                from s_user u
                         join s_order o on u.id = o.userID
                         JOIN s_order_details as od on od.orderID = o.id
                         JOIN s_articles_categories ac on ac.articleID = od.articleID
                         JOIN s_categories c on c.id = ac.categoryID
                where u.id = '.$userId;

        $userCategories = Shopware()->DB()->fetchAll($stmt);

        foreach ($userCategories  as $userCategory) {
            $explodedIds = explode('|', $userCategory['categoryPath']);
            $categoryIds = array_unique(array_merge($categoryIds, $explodedIds));
            $description[] = $userCategory[ 'description' ];
        }

        $categoryIdsAsStr = implode("|",$categoryIds);
        $descriptionAsStr = implode(",",array_unique($description));
        return $this->getSmartTags($categoryIdsAsStr, $descriptionAsStr);
    }

    /**
     * - getSmartTags fetches the eCommerceTags or categories for the given category path.
     *
     * @param string $categoryPath
     * @param string $description
     * @return string
     * @author Pradeep
     * @internal
     */
    public function getSmartTags(string $categoryPath, string $description):string
    {
        $smartTags = '';
        if(empty($this->shopCategories) || !isset($this->shopCategories)) {
            $this->shopCategories = $this->getCategoriesFromDB();
        }
        $categoryList = $this->shopCategories;
        if (empty($categoryList) || empty($categoryPath)) {
            return $smartTags;
        }
        $categoryIds = explode('|', $categoryPath);
        foreach ($categoryIds as $categoryId) {
            if (empty($categoryId) || (int)$categoryId <= 0) {
                continue;
            }
            $key = array_search($categoryId, array_column($categoryList, 'id'), true);
            $smartTags .= $categoryList[ $key ][ 'name' ];
            $smartTags .= ',';
        }
        $smartTags .= $description;
        return $smartTags;
    }


    /**
     * - getLastOrderId gets the last order Id of the orders table.
     * @return string
     * @author Pradeep
     */
    public function getLastOrderId():string
    {
        $totalNumberOfOrders = $this->getTotalNumberOfOrders();
        $this->logger->addLog('info', 'getLastOrderId total orders '.json_encode($totalNumberOfOrders));
        if(empty($this->shopOrders) || $totalNumberOfOrders <= 0 ){
            return '';
        }
        $lastOrderDetails = $this->shopOrders[$totalNumberOfOrders -1];
        $this->logger->addLog('info', 'getLastOrderId '.json_encode($lastOrderDetails));
        return $lastOrderDetails['order_no'];
    }

    public function getFirstOrderId():string
    {
        $totalNumberOfOrders = $this->getTotalNumberOfOrders();
        $this->logger->addLog('info', 'getFirstOrderId total orders '.json_encode($totalNumberOfOrders));
        if(empty($this->shopOrders) || $totalNumberOfOrders <= 0 ){
            return '';
        }
        $lastOrderDetails = $this->shopOrders[0];
        $this->logger->addLog('info', 'getFirstOrderId '.json_encode($lastOrderDetails));
        return $lastOrderDetails['order_no'];
    }

    /**
     * @param int $userId
     * @return string[]
     * @author Pradeep
     * @depreacated
     */
    public function getLatestOrderInformation(int $userId):array
    {
        $orderInfo = ['date' => '', 'order_no'=> '', 'net_value' => ''];
        $shopOrders = $this->shopOrders;
        if(empty($userId) || $userId <= 0 || empty($shopOrders)) {
            $this->logger->addLog('error', 'getLatestOrderInformation UserId '.json_encode($userId),__CLASS__,
                __METHOD__,__LINE__);
            return $orderInfo;
        }
        // Sort Orders based on Ordered Date,
        if(!isset($this->shopOrdersSortedWithDate) || empty($this->shopOrdersSortedWithDate)) {
            if(!usort($shopOrders,[$this, 'compareDate'])) {
                // Failed to sort Orders based on Ordered Date.
                $this->logger->addLog('error', 'Failed to sort Orders based on Ordered Date',__CLASS__,
                    __METHOD__,__LINE__);
                return $orderInfo;
            }
            $this->shopOrdersSortedWithDate = $shopOrders;
        }

        foreach($shopOrders as $order) {
            if((int)$order['user_id'] !== $userId) {
                continue;
            }
            // Get the 1st Order , since the orders are sorted with latest date.
            $orderInfo['date'] = $order['order_date'];
            $orderInfo['order_no'] = $order['order_no'];
            $orderInfo['net_value'] = $order['total_order_net_value'];
            break;
        }
        return $orderInfo;
    }

    /**
     * - getOrderNetValue gets order net value for the lastOrderNumber.
     * - net value is fetched from s_order table.
     * - net value is calculated from SUM(invoice_amount_net) - SUM(invoice_shipping_net)
     * @param string $last_order_number
     * @return float
     * @author Pradeep
     */
    public function getOrderNetValue(string $last_order_number):float
    {
        $netValue = 0.00;
        $stmt = '
                SELECT (invoice_amount_net) - (invoice_shipping_net) as total
                FROM `s_order` 
                WHERE `ordernumber` ='. $last_order_number;
        $result = Shopware()->DB()->fetchAll($stmt);
        if(empty($result) || !isset($result[0]['total'])) {
            return $netValue;
        }
        return round($result[0]['total'],2);
    }

    /**
     * - getFirstOrderDate gets the date on which firstOrder was made.
     * - date is fetched by doing SQL query to 's_orders' table
     * @param int $userId
     * @return string
     * @author Pradeep
     */
    public function getFirstOrderDate(int $userId): string
    {
        $stmt = '
                select ordertime as first_order_date
                from s_order
                where userID = ' . $userId . '
                  and ordernumber > 0
                order by id asc
                limit 1';
        $result = Shopware()->DB()->fetchAll($stmt);
        if(empty($result) || !isset($result[0]['first_order_date'])) {
            return '';
        }
        return $result[0]['first_order_date'];
    }

    /**
     * - getOrderIpAddress fetches ipaddress of the order based on orderNumber
     * - ipaddress is fetched from s_order table.
     * @param string $orderNumber
     * @author Pradeep
     */
    public function getOrderIpAddress(string $orderNumber): string
    {
        $stmt = '
                select remote_addr as ip
                from s_order
                where ordernumber =' . $orderNumber;
        $result = Shopware()->DB()->fetchAll($stmt);
        if(empty($result) || !isset($result[0]['ip'])) {
            return '';
        }
        return $result[0]['ip'];
    }

    /**
     * - compareDate compares the date information.
     * @param $element1
     * @param $element2
     * @return false|int
     * @author Pradeep
     */
    private function compareDate($element1, $element2) {
        $datetime1 = strtotime($element1['order_date']);
        $datetime2 = strtotime($element2['order_date']);
        return $datetime1 - $datetime2;
    }

    /**
     * - getLastYearOrderNetValue calculates the total net value of all the orders in last 12 months.
     * - The calculation is done directly from table 's_orders'
     * - returns net value of float type.
     * @param int $userId
     * @return array|float|int
     * @author Pradeep
     */
    public function getLastYearOrderNetValue(int $userId):float
    {
        $lastYear = $this->getLastYearStartAndEndDate();
        $startDate = $lastYear[ 'startDate' ];
        $endDate = $lastYear[ 'endDate' ];
        $netValue = 0;
        $stmt = '
                SELECT SUM(invoice_amount_net) - SUM(invoice_shipping_net) as total
                FROM `s_order` 
                WHERE 
                    `ordertime` >= "' . $startDate . '" AND 
                    `userID` = ' . $userId . ' AND 
                    `ordernumber` > 0';
        $this->logger->addLog('info', 'lastOrderNetValuSql', json_encode($stmt));
        $result = Shopware()->DB()->fetchAll($stmt);
        if(empty($result) || !isset($result[0]['total'])) {
            return 0.00;
        }
        return $result[0]['total'];
    }

    /**
     * - getLastYearStartAndEndDate calculates startdate and enddate
     * - The calculation is done based on 12 months minus from current month.
     * - returns array containing 'startDate' and 'endDate'
     * @return array
     * @author Pradeep
     */
    private function getLastYearStartAndEndDate(): array
    {
        $currentYear = date('Y');
        $lastYear = $currentYear - 1;
        $startDate = mktime(0, 0, 0, date('m'), date('d'), $lastYear) . "<br/>";
        $endDate = mktime(0, 0, 0, date('m'), date('d'), $currentYear) . "<br/>";
        return [
            'startDate' => date('Y-m-d', $startDate),
            'endDate' => date('Y-m-d', $endDate),
        ];
    }

    /**
     * - getTotalNumberOfOrders gets the total number of ShopOrders
     * - Used for synchronization of the shopOrders.
     * @return int
     * @author Pradeep
     */
    public function getTotalNumberOfOrders():int{
        if(!empty($this->shopOrders)) {
            return count($this->shopOrders);
        }
        return 0;
    }

    /**
     * - getCategoriesFromDB fetches all the categories of the products.
     * - categories are fetched from 's_categories' table.
     * @return array
     * @author Pradeep
     */
    private function getCategoriesFromDB()
    {
        $categories = [];
        $stmt = 'SELECT id, description as name FROM `s_categories` order by id ASC';
        $result = Shopware()->DB()->fetchAll($stmt);
        if (!empty($result)) {
            $categories = $result;
        }
        return $categories;
    }

    /**
     * - getTotalOrderNetValue calculates the total net value of all the orders.
     * - The calculation is done directly from table 's_orders'
     * - returns net value of float type.
     * @param int $userId
     * @return float|null
     * @author Pradeep
     */
    public function getTotalOrdersNetValue(int $userId):float
    {
        $netValue = 0.00;
        $stmt = '
                SELECT SUM(`invoice_amount_net`) - SUM(`invoice_shipping_net`) as netValue
                FROM `s_order` 
                WHERE 
                    `userID` = ' . $userId . ' AND 
                    `ordernumber` > 0';
        $result = Shopware()->DB()->fetchAll($stmt);
        if(empty($result) || !isset($result[0]['netValue'])) {
            return $netValue;
        }
        return $result[0]['netValue'];
    }
}