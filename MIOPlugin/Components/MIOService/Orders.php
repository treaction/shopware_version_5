<?php


namespace MIOPlugin\Components\MIOService;


class Orders extends MIO
{
    private const METHOD = 'POST';

    /**
     * - sync synchronizes the shop orders with AIO.
     * - returns boolean depending on the successful synchronization of the orders.
     * @param array $shopOrderDetails
     * @return bool
     * @author Pradeep
     */
    public function sync(array $shopOrderDetails): bool
    {
        $this->logger->addLog('info', 'sync ' . json_encode($shopOrderDetails));
        $status = false;
        $data[ 'base' ] = $this->getBaseData(self::ECHOOK);
        foreach ($shopOrderDetails as $shopOrderDetail) {
            $data[ 'contacts' ][] = $this->getContactMapping($shopOrderDetail);
        }
        $this->logger->addLog('info', 'syncOrderArray ' . json_encode($data), __CLASS__, __METHOD__, __LINE__);
        $data = $this->cURL->simpleEncode($data);
        $this->logger->addLog('info', 'base64Text : ' . json_encode($data), __CLASS__, __METHOD__,
            __LINE__);
        $response = $this->cURL->send($data, self::METHOD, self::CONTACTS_SYNC);
        $this->logger->addLog('info', 'Single Order Response: ' . json_encode($response), __CLASS__, __METHOD__,
            __LINE__);

        // $this->logger->addLog('info','syncOrderArray '.json_encode($syncOrderArray), __CLASS__, __METHOD__, __LINE__);
        return true;
    }

    /**
     * - getContactMapping is an helper function for sync.
     * - Maps the shopware fields with AIO fields before sending to AIO.
     *
     * @param array $shopOrderHistory
     * @return array
     * @author Pradeep
     */
    private function getContactMapping(array $shopOrderHistory): array
    {
        if (empty($shopOrderHistory)) {
            return [];
        }

        $contact[ 'standard' ] = [
            [
                'email' => $shopOrderHistory[ 'email' ] ?? '',
                'required' => 'true',
                'datatype' => 'Email',
                'regex' => '',
            ],
            [
                'salutation' => $shopOrderHistory[ 'salutation' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'first_name' => $shopOrderHistory[ 'firstname' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_name' => $shopOrderHistory[ 'lastname' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'birthday' => $shopOrderHistory[ 'birthday' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'street' => $shopOrderHistory[ 'street' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'house_number' => $shopOrderHistory[ 'street' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'postal_code' => $shopOrderHistory[ 'postal_code' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'city' => $shopOrderHistory[ 'city' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'country' => $shopOrderHistory[ 'country' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'smart_tags' => $shopOrderHistory[ 'smart_tags' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'total_order_net_value' => $shopOrderHistory[ 'total_orders_net_value' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_date' => $shopOrderHistory[ 'last_order_date' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_no' => $shopOrderHistory[ 'last_order_no' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_net_value' => $shopOrderHistory[ 'last_order_net_value' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_year_order_net_value' => $shopOrderHistory[ 'last_year_order_net_value' ] ?? '',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'first_order_date' => $shopOrderHistory['first_order_date']?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
            [
                'ip' => $shopOrderHistory['ip'] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
            [
                'doi_ip' => $shopOrderHistory['doi_ip'] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
            [
                'doi_timestamp' => $shopOrderHistory['doi_timestamp'] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
            [
                'optin_ip' => $shopOrderHistory['optin_ip'] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
            [
                'optin_timestamp' => $shopOrderHistory['optin_timestamp'] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => ''
            ],
        ];
        return $contact;
    }

    private function setEmail(string $email): bool
    {

    }
}