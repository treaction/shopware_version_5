<?php


namespace MIOPlugin\Components\MIOService;



class UserAccount extends MIO
{
    /**
     * @param string $salutation
     * @param string $firstName
     * @param string $lastName
     * @param string $eMail
     * @param string $permission
     * @return array
     * @author Pradeep
     */
    public function createAccount(
        string $salutation,
        string $firstName,
        string $lastName,
        string $eMail,
        string $permission
    ): array {
        $data = [
            'salutation' => $salutation,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $eMail,
            'permission' => $permission,
        ];


        $base64Text = $this->cURL->simpleEncode($data);
        $response = $this->cURL->send($base64Text, 'POST', self::ACCOUNT_CREATE_ENDPOINT);
        $this->logger->addLog('Info', json_encode($response), __CLASS__, __METHOD__, __LINE__);
        return $response;
    }
}