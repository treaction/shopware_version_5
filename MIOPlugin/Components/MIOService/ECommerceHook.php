<?php


namespace MIOPlugin\Components\MIOService;



class ECommerceHook extends MIO
{

    /**
     * @param array $ecPostData
     * @return bool
     * @author Pradeep
     */
    public function sendToECWebhook(array $ecPostData): bool
    {
        $method = 'POST';
        $endPoint = self::WEBHOOK_RECEIVE;
        if (empty($ecPostData)) {
            $this->logger->addLog('error ', 'Invalid Postdata provided', __CLASS__, __METHOD__, __LINE__);
            return false;
        }
        $ecFields = $this->mapToECFieldTypes($ecPostData);
        $this->logger->addLog('info ecFields ', json_encode($ecFields), __CLASS__, __METHOD__, __LINE__);
        $data = $this->cURL->simpleEncode($ecFields);
        $this->logger->addLog('info encoded data ', json_encode($data), __CLASS__, __METHOD__, __LINE__);
        $reqToNL = [
            'data' => $data,
            'method' => $method,
            'endpoint' => $endPoint,
        ];
        $this->logger->addLog('info', 'Webhook Ready to send Request ' . json_encode($reqToNL), __CLASS__, __METHOD__,
            __LINE__);
        $response = $this->cURL->send($data, $method, $endPoint);
        $this->logger->addLog('info', 'NL Webhook Response ' . json_encode($response), __CLASS__, __METHOD__, __LINE__);
        return true;
    }

    /**
     * @param array $ecPostData
     * @return array|array[]
     * @author Pradeep
     */
    private function mapToECFieldTypes(array $ecPostData): array
    {
        return [
            'base' => [
                'apikey' => $this->getAPIKey(),
                'account_number' => $this->getAccountNo(),
                'object_register_id' => $this->getObjectRegisterId(self::ECHOOK),
            ],
            'contact' => [
                'standard' => [
                    [
                        'email' => $ecPostData[ 'email' ] ?? '',
                        'required' => '',
                        'datatype' => 'Email',
                        'regex' => '',
                    ],
                    [
                        'salutation' => $ecPostData[ 'salutation' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'first_name' => $ecPostData[ 'first_name' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_name' => $ecPostData[ 'last_name' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'birthday' => $ecPostData[ 'birthday' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'street' => $ecPostData[ 'street' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'house_number' =>$ecPostData[ 'house_number' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'postal_code' => $ecPostData[ 'postal_code' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'city' => $ecPostData[ 'city' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'country' => $ecPostData[ 'country' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'smart_tags' => $ecPostData['smart_tags'] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'total_order_net_value' => $ecPostData['total_order_net_value'] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_date' => $ecPostData[ 'last_order_date' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_no' => $ecPostData[ 'last_order_number' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_net_value' => $ecPostData[ 'last_order_net_value' ] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_year_order_net_value' => $ecPostData['last_year_order_net_value'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'first_order_date' => $ecPostData['first_order_date'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'ip' => $ecPostData['ip'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'doi_ip' => $ecPostData['doi_ip'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'doi_timestamp' => $ecPostData['doi_timestamp'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'optin_ip' => $ecPostData['optin_ip'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                    [
                        'optin_timestamp' => $ecPostData['optin_timestamp'],
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                ],
                'custom' => '',
            ],
        ];
    }
}