<?php


namespace MIOPlugin\Components\MIOService;


class NewsletterHook extends MIO
{

    /**
     * @param array $nlPostData
     * @return bool
     * @author Pradeep
     * @internal Send POST request to AIO
     */
    public function sendToNLWebhook(array $nlPostData): bool
    {
        $method = 'POST';
        $endPoint = self::WEBHOOK_RECEIVE;

        if (empty($nlPostData)) {
            $this->logger->addLog('error ', 'Invalid Postdata provided', __CLASS__, __METHOD__, __LINE__);
            return false;
        }
        $this->logger->addLog('Info ', 'Post Data is valdiated ' . json_encode($nlPostData), __CLASS__, __METHOD__,
            __LINE__);
        $nlFields = $this->mapToNLFieldTypes($nlPostData);
        $this->logger->addLog('info', 'Mapped to NL FieldTypes ' . json_encode($nlFields), __CLASS__, __METHOD__,
            __LINE__);
        $data = $this->cURL->simpleEncode($nlFields);
        $reqToNL = [
            'data' => $data,
            'method' => $method,
            'endpoint' => $endPoint,
        ];
        $this->logger->addLog('info', 'Webhook Ready to send Request ' . json_encode($reqToNL), __CLASS__, __METHOD__,
            __LINE__);
        $response = $this->cURL->send($data, $method, $endPoint);
        $this->logger->addLog('info', 'NL Webhook Response ' . json_encode($response), __CLASS__, __METHOD__, __LINE__);

        return true;
    }

    /**
     * @param array $postData
     * @return array
     * @author Pradeep
     */
    public function mapToNLFieldTypes(array $postData): array
    {
        $this->logger->addLog('info', 'APIKey ' . $this->getAPIKey());
        $this->logger->addLog('info', 'getAccountNo ' . $this->getAccountNo());
        $this->logger->addLog('info', 'getObjectRegisterId ' . $this->getObjectRegisterId(self::NLHOOK));

        return [
            'base' => [
                'apikey' => $this->getAPIKey(),
                'account_number' => $this->getAccountNo(),
                'object_register_id' => $this->getObjectRegisterId(self::NLHOOK),
            ],
            'contact' => [
                'standard' => [
                    [
                        'email' => trim($postData[ 'newsletter' ]) ?? '',
                        'required' => '',
                        'datatype' => 'Email',
                        'regex' => '',
                    ],
                    [
                        'salutation' => trim($postData[ 'salutation' ]) ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'first_name' => trim($postData[ 'firstname' ]) ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_name' => trim($postData[ 'lastname' ]) ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                ],
                'custom' => '',
            ],
        ];
    }
}
