<?php


namespace MIOPlugin\Components\MIOService;


use MIOPlugin\Components\CURL;
use MIOPlugin\Components\PluginLogger;

class MIO
{
    protected const END_POINT_HOST = 'https://dev-api-in-one.net';
    public const NLHOOK = 'NewsletterHook';
    public const ECHOOK = 'eCommerceHook';
    public const ACCOUNT_CREATE_ENDPOINT = 'https://dev-api-in-one.net/v2.0/general/account/create';
    public const WEBHOOK_LIST_READ = self::END_POINT_HOST."/v2.0/webhook/list/read";
    public const WEBHOOK_RECEIVE = self::END_POINT_HOST."/v2.0/webhook/receive";
    public const VALIDIATE_APIKEY = self::END_POINT_HOST."/v2.0/general/validate/apikey";
    public const CONTACTS_SYNC =  self::END_POINT_HOST."/v2.0/contacts/shop/sync";
    // ENV could be DEV, TEST, PROD.
    // For ENV as PROD -> won't write any log entries
    public const ENV='DEV';



    /**
     * @var PluginLogger
     * @author Pradeep
     */
    protected $logger;
    /**
     * @var CURL
     * @author Pradeep
     */
    protected $cURL;
    /**
     * @var int
     * @author Pradeep
     */
    private $accountNo;
    /**
     * @var string
     * @author Pradeep
     */
    private $apikey;


    /**
     * MIOService constructor.
     */
    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->cURL = new CURL();
        //$this->validate = new ValidationService();
    }

    public function initialize(string $apikey, int $accountNo): bool
    {
        $this->logger->addLog('Info', 'Intialization process Started', __CLASS__, __METHOD__, __LINE__);
        $status = false;
        if ($this->isValidAccountNo($accountNo) && $this->isValidAPIKey($apikey)) {
            $this->apikey = $apikey;
            $this->accountNo = $accountNo;
            $this->logger->addLog('Info', 'Intialization Success', __CLASS__, __METHOD__, __LINE__);
            $status = true;
        }
        $this->logger->addLog('Info', 'Intialization Process Completed', __CLASS__, __METHOD__, __LINE__);
        return $status;
    }

    /**
     * @param int $accountNo
     * @return bool
     * @author Pradeep
     */
    private function isValidAccountNo(int $accountNo): bool
    {
        $this->logger->addLog('Info', 'Validating Account No', __CLASS__, __METHOD__, __LINE__);
        return !(empty($accountNo) || !is_int($accountNo));
    }

    /**
     * @param string $apikey
     * @return bool
     * @author Pradeep
     */
    private function isValidAPIKey(string $apikey): bool
    {
        $this->logger->addLog('Info', 'Validating APIKey', __CLASS__, __METHOD__, __LINE__);
        return !(empty($apikey) || !is_string($apikey));
    }



    /**
     * @return string|null
     * @author Pradeep
     */
    protected function getAPIKey(): ?string
    {
        if (!$this->isValidAPIKey($this->apikey)) {
            return null;
        }
        return $this->apikey;
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    protected function getAccountNo(): ?string
    {
        if (!$this->isValidAccountNo($this->accountNo)) {
            return null;
        }
        return $this->accountNo;
    }

    /**
     * @param string $webhookType
     * @return int|null
     * @author Pradeep
     */
    protected function getObjectRegisterId(string $webhookType): ?int
    {
        $objectregister_id = null;
        $supportedWebHooks = $this->getSupportedHooks();
        if (empty($webhookType) || !in_array($webhookType, $supportedWebHooks)) {
            $this->logger->addLog('error', 'Invalid Webhook Type');
            return $objectregister_id;
        }
        $webhooks = $this->getWebhooks();
        $this->logger->addLog('info', 'Webooks from GetObjRegId ' . json_encode(['Webhooks' => $webhooks]));

        if (empty($webhooks) || !$webhooks[ 'status' ] || empty($webhooks[ 'response' ])) {
            $this->logger->addLog('error', 'Failed to get Webhooks from MIO');
        }
        foreach ($webhooks[ 'response' ] as $webhook) {
            if ($webhook[ 'name' ] !== $webhookType || empty($webhook[ 'objectregister_id' ])) {
                continue;
            }
            $objectregister_id = $webhook[ 'objectregister_id' ];
            $this->logger->addLog('info', 'ObjRegId ' . json_encode(['Id' => $objectregister_id]));
        }
        return $objectregister_id;
    }

    /**
     * @param string $webhookName
     * @return array
     * @author Pradeep
     */
    protected function getBaseData(string $webhookName):array
    {
        return [
            'apikey' => $this->getAPIKey(),
            'account_number' => $this->getAccountNo(),
            'object_register_id' => $this->getObjectRegisterId($webhookName),
        ];
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    private function getSupportedHooks(): array
    {
        return [self::NLHOOK, self::ECHOOK];
    }

    /**
     * @author Pradeep
     */
    public function verifyAPIKeyWithAccountNumber()
    {
        $method="POST";
        $apikey = $this->getAPIKey();
        $accountNumber = $this->getAccountNo();

        if(empty($apikey) || empty($accountNumber)) {
            return [];
        }

        $endPoint = self::VALIDIATE_APIKEY;
        $body = [
            'base' => [
                'account_number' => $accountNumber,
                'apikey' => $apikey,
            ],
        ];
        $payload = base64_encode(json_encode($body));
        return $this->cURL->send($payload, $method, $endPoint, ['Content-Type: application/x-www-form-urlencoded']);
    }

    /**
     * @return array|mixed
     * @author Pradeep
     */
    public function getCompanyPrivacyPolicyFromMIO()
    {
       $response = $this->verifyAPIKeyWithAccountNumber();
        if (!isset($response[ 'response' ][ 'company' ], $response[ 'message' ]) ||
            empty($response) || !$response[ 'status' ]) {
            return [];
        }

        return $response[ 'response' ][ 'company' ];
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getWebhooks(): array
    {
        $webhooks = [];
        $method = 'POST';
        $apikey = $this->getAPIKey();
        $accountNo = $this->getAccountNo();
        $endPoint = self::WEBHOOK_LIST_READ;
        $body = [
            'base' => [
                'account_no' => $accountNo,
                'apikey' => $apikey,
            ],
        ];
        // AS PER THE SERVICE FROM AIO (data IS APPENDED TO THE BODY)
        $base64Text = 'data=' . base64_encode(json_encode($body));
        $this->logger->addLog('info', 'GetWebhook List base64Text :' . $base64Text, __CLASS__, __METHOD__, __LINE__);
        return $this->cURL->send($base64Text, $method, $endPoint, ['Content-Type: application/x-www-form-urlencoded']);
    }

    /**
     * @param array $array
     * @return string
     * @author Pradeep
     * @internal Convert
     */
    protected function toString(array $array): string
    {
        if (empty($array)) {
            return '';
        }
        return implode(',', $array);
    }
}