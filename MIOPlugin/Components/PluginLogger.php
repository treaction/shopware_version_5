<?php


namespace MIOPlugin\Components;


use DateTime;
use MIOPlugin\Components\MIOService\MIO;

class PluginLogger
{

    private CONST LOG_FILE_PATH = 'custom/plugins/MIOPlugin/log/dev.log';
    /**
     * @var DateTime
     * @author Pradeep
     */
    private $dateTime;
    /**
     * @var string
     * @author Pradeep
     */
    private string $env;

    public function __construct()
    {
        $this->dateTime = new DateTime();
        $this->env = MIO::ENV;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getLogLocation():string {
        return Shopware()->DocPath() . self::LOG_FILE_PATH;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getTimeStamp():string {
        $unixTimeStamp = $this->dateTime->getTimestamp();
        $this->dateTime->setTimestamp($unixTimeStamp);
        return $this->dateTime->format('Y-m-d H:i:s');
    }

    /**
     * @param string $type
     * @param string $message
     * @param string $className
     * @param string $method
     * @param string $line
     * @author Pradeep
     */
    public function addLog(string $type, string $message, string $className='', string $method='', string $line=''):void {
        if ($this->env !== 'PROD') {
            $destination = $this->getLogLocation();
            $currentTime = $this->getTimeStamp();
            $message = '['.$currentTime.']'.$type.':'.$message.'['.$className.','.$method.','.$line.']';
            error_log(print_r($message,true)."\n", 3, $destination);
        }
    }
}