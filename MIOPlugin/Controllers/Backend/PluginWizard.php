<?php


use Doctrine\ORM\Query;
use MIOPlugin\Components\MIOService\MIO;
use MIOPlugin\Components\MIOService\Orders;
use MIOPlugin\Components\MIOService\UserAccount;
use MIOPlugin\Components\PluginLogger;

//se MIOPlugin\Components\ValidationService;
use MIOPlugin\Components\ShopOrders;
use MIOPlugin\Components\ValidationService;
use MIOPlugin\Models\MIOOrdersSyncRepository;
use Shopware\Models\Customer\Customer;
use Shopware\Models\Order\Order;
use Shopware\Models\User\Repository;
use Shopware\Models\User\User;
use Symfony\Component\HttpFoundation\Response;

class Shopware_Controllers_Backend_PluginWizard extends Shopware_Controllers_Backend_ExtJs
{

    /**
     * @var PluginLogger|void
     * @author Pradeep
     */
    private $logger;
    /**
     * @var MIO|void
     * @author Pradeep
     */
    private $mioService;
    /**
     * @var ValidationService|void
     * @author Pradeep
     */
    private $validate;

    /**
     * @var Orders|void
     * @author Pradeep
     */
    private $shopOrders;
    /**
     * @var ShopOrders
     * @author Pradeep
     */
    private ShopOrders $orderHistory;
    /**
     * @var Orders
     * @author Pradeep
     */
    private Orders $mioOrders;
    /**
     * @var MIOOrdersSyncRepository
     * @author Pradeep
     */
    private MIOOrdersSyncRepository $mioOrdersSyncRepository;


    public function testAction()
    {
        $init = $this->initialize();
        $this->logger->addLog('info', 'TestAction Test ' . json_encode($init));
        $this->response->setHttpResponseCode(Response::HTTP_OK);
        $this->View()->assign('responseText', 'Installed');
    }

    /**
     * @author Pradeep
     */
    private function initialize(): bool
    {
        $return = false;
        $this->logger = new PluginLogger();
        $this->validate = new ValidationService();
        if (!is_null($this->logger) && !is_null($this->validate)) {
            $return = true;
        }
        return $return;
    }

    /**
     * - VerifyAction is used to validate APIKey with Account Number given in the plugin configuration tab.
     * - The Validation is done by sending the service request to 'verifyAPIKeyWithAccountNumber' in AIO
     * - The response from AIO will be updated automatically to view() to show response in frontend.
     * @author Pradeep
     */
    public function verifyAction()
    {

        if (!$this->initialize()) {
            $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
        }
       $this->logger->addLog('info', 'MIOApi Test ');
        // Get APIKey and AccountNo from Plugin
        $pluginInfo = Shopware()->Container()->get('shopware.plugin.config_reader')->getByPluginName('MIOPlugin');
        $apikey = $pluginInfo[ 'apikey' ] ?? '';
        $accountNo = $pluginInfo[ 'accountno' ] ?? '';
        // Initialize MIOService
        $mio = new MIO();
        try {
            if (!$mio->initialize($apikey, (int)$accountNo)) {
                throw new Exception('Failed to initialize MIOService');
            }
            $response = $mio->verifyAPIKeyWithAccountNumber();
            $this->logger->addLog('verify account info', json_encode($response));
            if (!isset($response[ 'response' ][ 'company' ], $response[ 'message' ]) ||
                empty($response) || !$response[ 'status' ]) {
                throw new Exception('Invalid APIKey Or AccountNo');
            }

            $responseText = $response[ 'message' ] ?? '';
            $this->response->setHttpResponseCode(Response::HTTP_OK);
        } catch (Exception $e) {
            $responseText = $e->getMessage();
            $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
        }
        $this->View()->assign('responseText', $responseText);
    }

    /**
     * - createAction creates a new account for MarketingInOne.
     * - Account is created by sending a request to AIO (createAccount)
     * - The response text is assigned to view to show result to frontend.
     * @author Pradeep
     */
    public function createAction(): void
    {
        try {
            if (!$this->initialize()) {
                $this->logger->addLog('info', 'createAction initialization failed');
                $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
            }
            $this->logger->addLog('info', 'createAction ');

            // Get APIKey and AccountNo from Plugin
            $pluginInfo = Shopware()->Container()->get('shopware.plugin.config_reader')->getByPluginName('MIOPlugin');
            $salutation = $this->validate->sanitizeString($pluginInfo[ 'salutation' ]);
            $firstname = $this->validate->sanitizeString($pluginInfo[ 'firstname' ]);
            $lastname = $this->validate->sanitizeString($pluginInfo[ 'lastname' ]);
            $email = $this->validate->sanitizeEmail($pluginInfo[ 'email' ]);
            $permission = $this->validate->sanitizeString($pluginInfo[ 'permission' ]);
            $this->logger->addLog('info','Permission '.json_encode($permission));
            if(!$permission || strtolower($permission) === "no" ) {
                throw new Exception('Please accept the Terms and Conditions.');
            }
            // Validate all Inputs
            $hasError = $this->validateInputs($salutation, $firstname, $lastname, $email, $permission);
            $this->logger->addLog('info', 'Validation HasError' . json_encode($hasError), __CLASS__, __METHOD__,
                __LINE__);
            if ($hasError[ 'error' ] || !empty($hasError[ 'errMessage' ])) {
                throw new Exception($hasError[ 'errMessage' ]);
            }
            $mioUserAccount = new UserAccount();
            $response = $mioUserAccount->createAccount($salutation, $firstname, $lastname, $email, $permission);
            $this->logger->addLog('info', 'Create User Account ' . json_encode($response), __CLASS__, __METHOD__,
                __LINE__);

            // validating Response
            if (empty($response) || empty($response[ 'status' ]) || empty($response[ 'message' ])) {
                throw new Exception('Failed to register an Account.');
            }
            if (!$response[ 'status' ] && !empty($response[ 'message' ])) {
                throw new Exception($response[ 'message' ]);
            }
            $responseText = $response[ 'message' ];
            $this->View()->assign('responseText', $responseText);
            $this->response->setHttpResponseCode(Response::HTTP_OK);
        } catch (Exception $e) {
            $this->logger->addLog('exception', $e->getMessage(), __CLASS__, __METHOD__, __LINE__);
            $responseText = $e->getMessage();
            $this->View()->assign('responseText', $responseText);
            $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * - syncAction synchronizes customer orders to MIO.
     * - Synchronization is done by sending request in AIO (sync).
     * - After the synchronization is completed, the result or status is stored in Database.
     * - The response of the result is also updated to view() to display in frontend.
     * @author Pradeep
     */
    public function syncAction():void
    {
        if (!$this->initialize()) {
            $this->logger->addLog('info', 'createAction initialization failed');
            $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
        }
        $this->logger->addLog('info', 'createAction ');

        // Get APIKey and AccountNo from Plugin
        $pluginInfo = Shopware()->Container()->get('shopware.plugin.config_reader')->getByPluginName('MIOPlugin');
        $apikey = $pluginInfo[ 'apikey' ] ?? '';
        $accountNo = $pluginInfo[ 'accountno' ] ?? '';
        // Initialize MIOService
        try {
            if(!$this->initSyncContacts()) {
                throw new Exception("Failed to Initialize service");
            }
            if (!$this->mioOrders->initialize($apikey, (int)$accountNo)) {
                throw new Exception('Synchronization Failed: Invalid APIKey or Account Number provided');
            }
            $webhooks = $this->mioOrders->getWebhooks();
            if (empty($webhooks) || !$webhooks[ 'status' ] || empty($webhooks[ 'response' ])) {
                throw new Exception('Synchronization Failed: Invalid APIKey Or AccountNo');
            }
            // Check if contacts are already synchronized with MIO.
            $isShopOrdersSynced = $this->mioOrdersSyncRepository->isAccountNumberExists($accountNo);
            if (!$isShopOrdersSynced) {
                // Get Total ShopOrders
                $shopOrders = $this->orderHistory->getShopOrderHistory();
                // Send Sync request to MIO
                $responseText = "Successufully synchronized contacts to MIO";
                $this->View()->assign('responseText', $responseText);

                // Todo Remove before deployment.
                $this->mioOrders->sync($shopOrders);
                $lastShopOrderId = $this->orderHistory->getLastOrderId();
                $firstShopOrderId = $this->orderHistory->getFirstOrderId();
                $orderCount = $this->orderHistory->getTotalNumberOfOrders();
                // Update shopware DB with status of the sync
                $this->mioOrdersSyncRepository->insert(
                    $accountNo,
                    $lastShopOrderId,
                    $firstShopOrderId,
                    $orderCount,
                    true
                );
            }
            $responseText = "Successufully synchronized contacts to MIO";
            $this->View()->assign('responseText', $responseText);
            $this->response->setHttpResponseCode(Response::HTTP_OK);
        } catch (Exception $e) {
            $this->logger->addLog('exception', $e->getMessage(), __CLASS__, __METHOD__, __LINE__);
            $responseText = $e->getMessage();
            $this->View()->assign('responseText', $responseText);
            $this->response->setHttpResponseCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function initSyncContacts()
    {
        $this->orderHistory = new ShopOrders();
        $this->mioOrders = new Orders();
        $this->logger = new PluginLogger();
        $this->mioOrdersSyncRepository = new MIOOrdersSyncRepository();

        if ($this->orderHistory === null || $this->mioOrders === null) {
            $this->logger->addLog('error', 'failed to initialize order or shopOrders');
            return false;
        }
        return true;
    }

    /**
     * @param string $salutation
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $permission
     * @return array
     * @author Pradeep
     */
    private function validateInputs(
        string $salutation,
        string $firstname,
        string $lastname,
        string $email,
        string $permission
    ): array {
        try {
            if (!$this->validate->isValidSalutation($salutation)) {
                throw new Exception('Invalid Salutation provided');
            }
            if (!$this->validate->isValidName($firstname) || !$this->validate->isValidName($lastname)) {
                throw new Exception('Invalid FirstName or LastName provided');
            }
            if (!$this->validate->isValidEmail($email)) {
                throw new Exception('Invalid E-Mail provided');
            }
            if (!$this->validate->isValidPermission($permission)) {
                throw new Exception('Invalid Permission provided');
            }
            return [
                'error' => false,
                'errMessage' => '',
            ];
        } catch (Exception $e) {
            $this->logger->addLog('exception', $e->getMessage(), __CLASS__, __METHOD__, __LINE__);
            return [
                'error' => true,
                'errMessage' => $e->getMessage(),
            ];
        }
    }
}