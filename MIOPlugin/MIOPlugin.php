<?php

namespace MIOPlugin;

use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use MIOPlugin\Models\MIOOrdersSync;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

class MIOPlugin extends Plugin
{
    /**
     * @param InstallContext $context
     * @throws ToolsException
     * @author Pradeep
     */
    public function install(InstallContext $context)
    {
        $em = $this->container->get('models');
        $tool = new SchemaTool($em);
        $classes = [$em->getClassMetadata(MIOOrdersSync::class)];
        $tool->createSchema($classes);
    }

    /**
     * @param UninstallContext $context
     * @author Pradeep
     */
    public function uninstall(UninstallContext $context)
    {
        $em = $this->container->get('models');
        $tool = new SchemaTool($em);
        $classes = [$em->getClassMetadata(MIOOrdersSync::class)];
        $tool->dropSchema($classes);
    }
}